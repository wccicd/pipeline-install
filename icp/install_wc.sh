#!/bin/bash
#
# Licensed Materials - Property of HCL
#
# Author: Rick henriksen
#
# * This installer will load WC images into an ICP docker registry and then deploy vault/consol followed by a demo WC authoring environment.


HOSTNAME=$(/bin/hostname)
IP_ADDR=$(ip route get 1 | head -1 | awk '{print $7}')
ICP_USER=admin
ICP_PW=admin
DOCKER_USER=admin
DOCKER_PW=admin
DOCKER_REG=mycluster.icp:8500
ICP_NAMESPACE=commerce

PROGNAME=$(basename $0)

echo "logging in to ICP"
cloudctl login -a https://$IP_ADDR:8443 --skip-ssl-validation -u $ICP_USER -p $ICP_PW -n default
echo "creating commerce namespace..."
kubectl create namespace commerce
cloudctl target -n commerce

echo "un-tarring cloudpak..."
tar xvf *.tgz

echo "un-tarring charts..."
cd charts
for i in `ls`; do
   tar xvf $i
done
cd ..

echo "loading WC images into the ICP docker registry..."
echo "Note: this will take several minutes"
> temp.txt
for i in `ls images/*.tar.gz`; do
   echo loading $i
   docker load -i $i >> temp.txt
done
cut -f 3 -d " " temp.txt > imagelist.txt
docker login $DOCKER_REG -u $DOCKER_USER -p $DOCKER_PW
while read imagename; do
   docker tag $imagename $DOCKER_REG/$ICP_NAMESPACE/$imagename
   docker push $DOCKER_REG/$ICP_NAMESPACE/$imagename
done < imagelist.txt

echo "creating commerce role based access control, security policies, etc...."
kubectl create -f rbac.yaml -n commerce
kubectl create -f security.yaml -n commerce
kubectl -n commerce create role wcs-psp --verb=use --resource=podsecuritypolicy --resource-name=commerce-psp
kubectl -n commerce create rolebinding default:wcs-psp --role=wcs-psp --serviceaccount=commerce:default

echo "Deploying Vault and Consul..."
helm install --name vault-consul ./charts/ibm-websphere-commerce-vaultconsul --tls --wait

sed -i "s#license: not_accepted#license: accept#" ./charts/ibm-websphere-commerce/values.yaml

echo "Deploying Commerce..."
helm install --name demoqaauth ./charts/ibm-websphere-commerce --tls --wait --timeout 1200

echo "Creating search index..."
echo "$IP_ADDR tsapp.demoqaauth.ibm.com" >> /etc/hosts
echo "$IP_ADDR search.demoqaauth.ibm.com" >> /etc/hosts
job_id=`curl -k -s -X POST -u spiuser:passw0rd https://tsapp.demoqaauth.ibm.com/wcs/resources/admin/index/dataImport/build?masterCatalogId=10001 | cut -f 2 -d ":" | sed 's/"//g' | sed 's/}//'`
keep_checking=1
while [ $keep_checking -eq 1 ]; do
   status=$(curl -k -u spiuser:passw0rd -X GET https://search.demoqaauth.ibm.com/search/admin/resources/index/build/status?jobStatusId=$job_id 2>/dev/null| grep -c 'Indexing job finished successfully')
   if [ $status -eq 1 ]; then
      echo "index is finsihed being built."
      keep_checking=0
   else
      echo "index is still building.  will check again in 10 seconds..."
      sleep 10
   fi
done