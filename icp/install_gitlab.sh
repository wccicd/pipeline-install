#install docker prereq's and docker itself:

#start gitlab container
host=`hostname`
external_url="http://$host:30080"
sudo docker run --detach --name gitlab \
        --volume /srv/gitlab/config:/etc/gitlab \
        --volume /srv/gitlab/logs:/var/log/gitlab \
        --volume /srv/gitlab/data:/var/opt/gitlab \
        --restart always \
	--hostname $host \
	--publish 30080:30080 \
         --publish 30022:22 \
	--env GITLAB_OMNIBUS_CONFIG="external_url \"$external_url\"; gitlab_rails['gitlab_shell_ssh_port']=30022;" \
	gitlab/gitlab-ce:latest

echo "Waiting 5 minutes for gitlab to start..."
sleep 300
echo "Done sleeping."