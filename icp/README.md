#### The following steps can be used to create a fully functional CI/CD pipleline which can be used to perform automated build and deploys for Websphere Commerce:

# Prepare git virtual machine:

1.  Procure a virtual machine running Centos or RHEL 7.7+, 1+ cores,
    4GB+ memory

2.  Execute all instructions as root:

    ***yum -y update;*** ***yum -y install git*** (update all packages)

3.  reboot

4.  Login as root

1.  git clone CI/CD installation scripts repo: `git clone
    https://gitlab.com/wccicd/pipeline-install.git`

# Install Docker:
1. Follow the necessary steps to install Docker for each release CentOS [CentOs8](centos_docker.md), [Redhat docker install](redhat_docker.md)

5.  run script which will install docker, disable the firewall, and
    install gitlab:

    1.  ***cd pipeline-install***

    2.  inspect and update the environment variables in
        install\_gitlab.sh as appropriate for your environment.

    3.  ***./install\_gitlab_prereqs.sh***
    4.  ***reboot***
    5.  ***ssh into server using port 20220 from this point forward***
    6.  ***Log in as root***
    7.  ***cd pipeline-install***
    8.  ***./install\_gitlab.sh***
    9.  *** Note: there will be about a 5 minute delay waiting for gitlab to start ***

6.  Using a web browser, go to ***host name\>:30080***

7.  Set the admin password and note it

8.  Register a new user account such as “wccicd”. This user will own the
    CI/CD repositories as well as be use to access the repo’s and update
    them during pipeline execution

9.  Create new projects with “internal” access level and the with the following names: wcbuild, helmcharts, crs,
    ts-app, xc-app, ts-utils, ts-web, search-app.


10. For each project, go to Settings / CI/CD, expand the AutoDevops
    section and disable auto devops.

11. On the git VM, as root

    4.  Generate an ssh key:
        ***ssh-keygen -t ed25519 -C "wccicd@test.com"***

    5.  Hit enter 3 times

    6.  ***cat*** ***\~/.ssh/id\_ed25519.pub***

    7.  Select the output from the above and copy to your clipboard.

12. On your gitlab web browser, click on the user icon in the upper
    right corner, and then on settings. Click on SSH Keys on the left
    panel. In the Key box, paste in the contents of the clipboard. Click
    on “add key”.

13. Execute the copy\_repos.sh script: ***./copy\_repos.sh*** (this will
    take the OOB CI/CD pipeline repositories and populate them into your
    own git repos). Respond “Yes” to any prompts.

# Prepare ICP virtual machine:

1.  Procure a virtual machine running Centos or RHEL 7.7+, 12+ cores,
    48GB+ memory

2.  As the root user:

    1.  ***yum -y update;*** ***yum -y install git*** (update all
        packages, install git)

3.  ***reboot***

1.  Login as root

4.  git clone CI/CD installation scripts repo: `git clone
    http://gitlab.com/wccicd/pipeline-install.git`

5.  ***cd*** ***pipeline-install***

6.  inspect and update the environment variables in install\_icp.sh as
    appropriate for your environment.

7.  ***./install\_icp.sh*** (for redhat you will have to use the guide in: [redhat_icp_install.sh](redhat_icp_install.sh)

8.  Note: it will take 20-30 minutes for the ICP install to complete

9.  After this completes, verify that you can log into the ICP console.
    Go to ***VM ip address\>:8443*** and log in with the credentials of
    admin/admin.

10. Install Commerce:

    2.  From HCL Flexnet, download the WC ICP cloudpack and place the
        single large .tgz file into the current directory
        (./pipeline-install). Then, continue with the next step.

    3.  Inspect and update the environment variables in install\_wc.sh
        as appropriate for your environment.

    4.  ***./install\_wc.sh***

    5.  Note: this may take up to 20 minutes to complete

    6.  Please update your local host file to have ***“\<i.p. address of
        ICP server\>*** ***store.demoqaauth.ibm.com***”

    7.  Using a web browser, navigate to
        [***https://store.demoqaauth.ibm.com/wcs/shop/en/auroraesite***](https://store.demoqaauth.ibm.com/wcs/shop/en/auroraesite)

    8.  It may take a minute or so for the home page to come up
        initially. The aurora home page should come up and it should
        also display product categories from the search index. This is
        validation that your WC environment has been deployed properly.

# Prepare Jenkins virtual machine:

1.  Procure a virtual machine running Centos or RHEL 7.7+, 2+ cores,
    8GB+ memory

2.  ***yum -y update; yum -y install git***

3.  ***reboot***

4.  git clone CI/CD installation scripts repo: `git clone
    http://gitlab.com/wccicd/pipeline-install.git

5.  Inspect and update the install\_jenkins.sh environment variables as
    appropriate for your environment.

6.  ***./install\_jenkins.sh***. Respond “yes” to any prompts saying
    “Are you sure you want to continue connecting”…

7.  Using a web browser, navigate ***to server i.p.\>:8080***

8.  Log in using jenkins/passw0rd

9.  Click on “Install suggest plugins”, wait for completion.

10. Click on “save and finish”

11. Click on “start using Jenkins”

12. Click on “open blue ocean”

13. Click “Create a new pipeline”

14. Click “Git”

15. Enter “***ssh://git@testgit.localdomain:30022/wccicd/wcbuild.git***”
    into the repository url field.  Note: adjust the host name to match the host
    name of your git server.

16. Paste the ssh key text from the next box into your git repo SSH
    keys:

    1.  Select the text from the Jenkins key field and copy to the
        clipboard.

    2.  In git, Click on the icon in the upper right, settings

    3.  On the left nav, click SSH keys

    4.  Paste the clipboard key text into the key box.

    5.  Click on “Add Key”

17. Back to Jenkins, click on “create pipeline”

18. At this point the pipeline job should execute and create the wcbuild
    container. It will take several minutes

19. Once it completes, you should have a green successful screen. Click
    on the “x” in the upper right to close that screen.

20. Click “Pipelines” on the top menu followed by “New Pipeline”

21. Click on git

22. Paste “***ssh://git@testgit.localdomain:30022/wccicd/crs.git***”
    into the git url field.  Note: adjust the host name to match the host namne
    of your git server.

23. Click “Create pipeline”

24. Log into icp VM and remove tempuser user-id.
