ICP_IP=192.168.0.22
GIT_IP=192.168.0.20
GIT_HOST=testgit.localdomain
ICP_HOST=mycluster.icp
ICP_PORT=8443
ICP_DOCKER_PORT=8500
ICP_USER=admin
ICP_PW=admin

#Install Jenkins:
echo "updating all system packages..."
yum -y update
echo "retrieving jenkins war..."
yum -y install wget unzip zip
wget http://mirrors.jenkins.io/war-stable/latest/jenkins.war
adduser jenkins
groupadd jenkins
mkdir -p /opt/jenkins
mv jenkins.war /opt/jenkins
chown -R jenkins:jenkins /opt/jenkins
echo "installing java..."
yum -y install java
echo "creating jenkins service..."
cat <<EOF > /etc/systemd/system/jenkins.service
[Unit]
Description=Jenkins Daemon

[Service]
ExecStart=/usr/bin/java -jar /opt/jenkins/jenkins.war
User=jenkins

[Install]
WantedBy=multi-user.target
EOF

echo "starting jenkins..."
systemctl daemon-reload
systemctl enable jenkins
systemctl start jenkins
echo "sleeping to give jenkins time to start..."
sleep 30
echo "Stopping firewall..."
systemctl stop firewalld
systemctl disable firewalld
adminpw=`cat /home/jenkins/.jenkins/secrets/initialAdminPassword`

#Install docker
echo "installing docker pre-reqs..."
yum install -y yum-utils \
  device-mapper-persistent-data \
  lvm2
yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo
echo "installing docker..."
yum -y install docker-ce-18.06.2.ce-3.el7 containerd.io
echo "starting docker..."
systemctl start docker
systemctl enable docker

# add Jenkins user to docker group
usermod -a -G docker jenkins

#download the jenkins cli:
echo "downloading jenkins cli..."
wget http://localhost:8080/jnlpJars/jenkins-cli.jar
mv jenkins-cli.jar /opt/jenkins
chown jenkins:jenkins /opt/jenkins/jenkins-cli.jar
echo "creating jenkins user jenkins:passw0rd"
echo 'jenkins.model.Jenkins.instance.securityRealm.createAccount("jenkins", "passw0rd")' | java -jar /opt/jenkins/jenkins-cli.jar -s "http://localhost:8080" -auth admin:$adminpw -noKeyAuth groovy = –
plugin_list="pipeline-milestone-step cloudbees-folder branch-api jdk-tool script-security momentjs command-launcher gradle antisamy-markup-formatter docker-commons structs workflow-step-api token-macro pipeline-stage-view bouncycastle-api build-timeout docker-workflow credentials pipeline-build-step plain-credentials ssh-credentials pipeline-model-api credentials-binding scm-api workflow-api timestamper workflow-support durable-task workflow-durable-task-step junit matrix-project jsch resource-disposer github-api ws-cleanup ant ace-editor jquery-detached git-client workflow-scm-step git workflow-cps workflow-job display-url-api github mailer apache-httpcomponents-client-4-api git-server workflow-basic-steps jackson2-api workflow-cps-global-lib pipeline-input-step pipeline-stage-step pipeline-graph-analysis pipeline-rest-api handlebars pipeline-model-extensions blueocean-display-url workflow-multibranch blueocean-config authentication-tokens pipeline-stage-tags-metadata blueocean-git-pipeline pipeline-model-declarative-agent mercurial pipeline-model-definition lockable-resources workflow-aggregator sse-gateway github-branch-source pipeline-github-lib mapdb-api subversion ssh-slaves matrix-auth pam-auth ldap email-ext blueocean-events gitlab-plugin windows-slaves ruby-runtime gitlab-hook blueocean-i18n blueocean-commons blueocean-autofavorite blueocean-jwt blueocean-rest pubsub-light blueocean-pipeline-scm-api htmlpublisher handy-uri-templates-2-api jenkins-design-language blueocean blueocean-core-js variant blueocean-web favorite ssh-agent blueocean-rest-impl blueocean-pipeline-api-impl blueocean-dashboard blueocean-pipeline-editor jira pollscm blueocean-jira blueocean-bitbucket-pipeline blueocean-executor-info blueocean-github-pipeline cloudbees-bitbucket-branch-source blueocean-personalization"

echo "Installing Jenkins Plugins necessary for a CI/CD pipeline..."
java -jar /opt/jenkins/jenkins-cli.jar -s "http://localhost:8080" -auth jenkins:passw0rd install-plugin $plugin_list

echo "restarting jenkins..."
systemctl restart jenkins

echo "installing cloudctl..."
echo "$ICP_IP $ICP_HOST" >> /etc/hosts
curl -kLo cloudctl-linux-amd64-3.1.1-973 https://$ICP_IP:$ICP_PORT/api/cli/cloudctl-linux-amd64
chmod 755 cloudctl-linux-amd64-3.1.1-973
mv cloudctl-linux-amd64-3.1.1-973 /usr/local/bin/cloudctl

echo "installing kubectl..."
curl -kLo kubectl-linux-amd64-v1.11.1 https://$ICP_IP:$ICP_PORT/api/cli/kubectl-linux-amd64
chmod 755 kubectl-linux-amd64-v1.11.1
mv kubectl-linux-amd64-v1.11.1 /usr/local/bin/kubectl

echo "installing helm client..."
curl -kLo helm-linux-amd64-v2.9.1.tar.gz https://$ICP_IP:$ICP_PORT/api/cli/helm-linux-amd64.tar.gz
mkdir helm-unpacked
tar -xvzf ./helm-linux-amd64-v2.9.1.tar.gz -C helm-unpacked
chmod 755 ./helm-unpacked/linux-amd64/helm
mv ./helm-unpacked/linux-amd64/helm /usr/local/bin/helm
rm -rf ./helm-unpacked ./helm-linux-amd64-v2.9.1.tar.gz

echo "authenticating to ICP..."
cloudctl login -a https://$ICP_IP:$ICP_PORT --skip-ssl-validation -u $ICP_USER -p $ICP_PW -n default

echo "copying ICP certificate for docker cli..."
cat <<EOF2 > tempuser_pk
-----BEGIN OPENSSH PRIVATE KEY-----
b3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAABFwAAAAdzc2gtcn
NhAAAAAwEAAQAAAQEA10mrtbPuq4dsVhKAIPACfI5Udr11sy/kAymGxZjMi76DJpBg+gI5
cLBNszmd3sDKUdc7MgvYgywgQs9TN6YPvfpjcbM8jyskUcTqCE8IJ/sB7KZuk5Z8E44fcM
cvef3IlDv6J78YS9vgOy1FslUI8VF9doP3WdEg1Up6ZjHqqksrONZVjcB7VtFiTb8o7CAn
qEbNkmL4WRydCudSIPR1IUKstXemugFiZaSwyM3ZdR+I7JGRiGRmnfhsmKMYahJPyh4ohi
iM3PoOa6FElryrg8YwG9+6e5lG5a5KDgVzDOZ+uCM1gDm0DeDHBGU+MKwwEO4CMUwt78hi
Q4MpDNbuPwAAA+CCiD9Ogog/TgAAAAdzc2gtcnNhAAABAQDXSau1s+6rh2xWEoAg8AJ8jl
R2vXWzL+QDKYbFmMyLvoMmkGD6AjlwsE2zOZ3ewMpR1zsyC9iDLCBCz1M3pg+9+mNxszyP
KyRRxOoITwgn+wHspm6TlnwTjh9wxy95/ciUO/onvxhL2+A7LUWyVQjxUX12g/dZ0SDVSn
pmMeqqSys41lWNwHtW0WJNvyjsICeoRs2SYvhZHJ0K51Ig9HUhQqy1d6a6AWJlpLDIzdl1
H4jskZGIZGad+GyYoxhqEk/KHiiGKIzc+g5roUSWvKuDxjAb37p7mUblrkoOBXMM5n64Iz
WAObQN4McEZT4wrDAQ7gIxTC3vyGJDgykM1u4/AAAAAwEAAQAAAQEAveyoJuxNqdUWpfg8
QNJ73xZRPU92/lVJeeCwnLFoAgBYAzVr2CMOOXPPmEVEGLdLxVdzSXtZzp6m90mfyIAfcy
HpiOzuh/8vU1O8gwf10g+GaxeIhXtEipve8MdIAv74bI1c6qOS85lN/+a4jdKmyP1zCZLy
K7JqnL/Pq0huFTwM6MP0tm3+tlvE5OduJT0eN8UH0nm8Rt0HQr7PsWHbbfQt2ZV7FpMRHn
TouXzmQZiEzobqc1buYr8GlQvCEDDzqhn83+Z508643gRMHkjPdvq6WUi8dVnr3txvCqs9
UlmzzryBkqhPQMSHhCuWeYL1umFivGCwd2dcXQPwcOlcAQAAAIAq5S/ywBE433B3l1iZvz
I/Z1dlKvtH5kDQli15vovu4MlKQOo3kSm4oTJleHL1R4RRbFXhkqwxkkeWU87/yRE0qM/u
2t6yutZ8G1/TttZS9JhyNxVPH5BTuxnN/k8fnE9fIGp3DfQmrKZl20FnFTkAET6+HkznZh
NrZBANdpgIvgAAAIEA7nsdRMZsWcS99SJVp0oIDg4ajFO0GeU9WYxAgTbHsKgzHJs/Zks1
I0ros1vc60OefcUwdBEXLFTL6J5QKSZeHAyOM4KlU1SQ1/QJV5BUZYA3AdSP0eYdgkhbht
jiqcWugjUfGTl1lpkwtdrL+woEXRk6EAhl5pYRGPyz32ZsOP8AAACBAOcaYon8D6LhMwJU
6I3eZ5LDADnqH0VRSzR5+/82HUQ1Q3C6UNB6ox9Arkj0MhLu80Dk8f1c8OiO5bMbgTNdaE
9AGv7seRYQib7b7FQqIIRb28JBxcUZTHAXrAl9fpEJn3bcI2MpNxa4gWn/5HP58hvhG22w
TjABp+VmjBEO+grBAAAAJXJpY2toZW5yaWtzZW5AUmlja3MtTWFjQm9vay1Qcm8ubG9jYW
wBAgMEBQ==
-----END OPENSSH PRIVATE KEY-----
EOF2
chmod 600 tempuser_pk
mkdir -p /etc/docker/certs.d/$ICP_HOST:$ICP_DOCKER_PORT
scp -i tempuser_pk tempuser@$ICP_HOST:/tmp/ca.crt /etc/docker/certs.d/$ICP_HOST:$ICP_DOCKER_PORT
chmod 644 /etc/docker/certs.d/$ICP_HOST:$ICP_DOCKER_PORT/ca.crt
systemctl restart docker
docker login $ICP_HOST:$ICP_DOCKER_PORT -u $ICP_USER -p $ICP_PW

echo "adding git ip to /etc/hosts"
echo "$GIT_IP $GIT_HOST" >> /etc/hosts