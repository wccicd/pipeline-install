#install docker prereq's and docker itself:
yum install -y yum-utils \
  device-mapper-persistent-data \
  lvm2
yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo
yum -y install docker-ce docker-ce-cli containerd.io
systemctl start docker
systemctl enable docker

#stop firewall:
systemctl disable firewalld
systemctl stop firewalld

#Big warning as it mayhave implications on how you use the server
echo ""
echo "......WAITING FOR USER CONFIRMATION......."
echo ""
echo ""
echo "We are about to change the settings for this VM sshd from"
echo "port 22 to port 20220, if you do not want to do this exit now"
echo "by pressing CRTL-C.."
echo ""

read aVariable

#Update sshd port so that gitlab doesn't conflict with it:
echo "Port 20220" >> /etc/ssh/sshd_config
echo "NOTE: Please connect to this server using port = 20220 in the future!!!!"
semanage port -a -t ssh_port_t -p tcp 20220
systemctl restart sshd.service
echo "REBOOT System before proceeding with next step!!!"
