#!/bin/bash
#
# Licensed Materials - Property of IBM
# 5737-E67
# @ Copyright IBM Corporation 2016, 2018 All Rights Reserved
# US Government Users Restricted Rights -
# Use, duplication or disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
#
# Author: Brian McSheehy <bmcshee@us.ibm.com>, updated by Rick Henriksen
#
# * This installer is only certified to work on a single node with Ubuntu 16.04x
# * The recommended minimum configuration for a single node ICP CE instance is 16 Core x 32 GB RAM x 25 GB Storage


TARGET_OS='CentOS Linux release 7.7.*'
PYTHON_VERSION='2.7'
VM_MAX_MAP_COUNT_SET='262144'
HOSTNAME=$(/bin/hostname)
IP_ADDR=$(ip route get 1 | head -1 | awk '{print $7}')
ICP_USER=admin
ICP_PW=admin
DOCKER_USER=admin
DOCKER_PW=admin
DOCKER_REG=mycluster.icp:8500
ICP_NAMESPACE=commerce

PROGNAME=$(basename $0)

APT="/bin/yum"

function error_exit
{
    echo "Error: ${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

echo "************************************************"
echo "         Starting IBM ICP-CE Installer          "
printf "************************************************\n\n"


# Check for OS Compatability
OS_CHECK=$(grep -c "$TARGET_OS" /etc/centos-release)

if [ $OS_CHECK != 1 ]; then
    error_exit "$LINENO: Unsupported operating system"
else
    printf "OS Compatability check \t[ OK ]\n"
fi


# Update yum repo
echo "Updating yum repositories"

$APT -y update

if [ -f /usr/bin/python ]; then
    PYTHON_V=`python -c "import sys;t='{v[0]}.{v[1]}'.format(v=list(sys.version_info[:2]));sys.stdout.write(t)";`

    if [ $PYTHON_V != $PYTHON_VERSION ]; then
        echo "Python $PYTHON_VERSION not found - installing"
        $APT install -y python
    else
        printf "Python $PYTHON_VERSION detected \t[ OK ]\n"
    fi

else
    echo "Python $PYTHON_VERSION not found - installing"
    $APT install -y python
fi


# Update sysctl vm.max_map_count

vm_max_map_count=$(sysctl vm.max_map_count | awk -F= '{print $2}' | tr -d '[:space:]')

if [ $vm_max_map_count != $VM_MAX_MAP_COUNT_SET ]; then
    echo "setting sysctl vm.max_map_count -> $VM_MAX_MAP_COUNT_SET"
    echo "vm.max_map_count=$VM_MAX_MAP_COUNT_SET" | tee -a /etc/sysctl.conf
    /sbin/sysctl -w vm.max_map_count=$VM_MAX_MAP_COUNT_SET
else
    printf "VM MAX MAP COUNT \t[ OK ]\n"
fi


# Update sysctl net.ipv4.ip_local_port_range

IPV4_PORT_RANGE=$(grep -c "^net.ipv4.ip_local_port_range" /etc/sysctl.conf)

if [ $IPV4_PORT_RANGE == 0 ]; then
    echo "setting sysctl net.ipv4.ip_local_port_range -> 10240  60999"
    echo 'net.ipv4.ip_local_port_range="10240 60999"' | tee -a /etc/sysctl.conf
    /sbin/sysctl -w net.ipv4.ip_local_port_range="10240  60999"
else
    printf "IPV4 Port Range \t[ OK ]\n"
fi


# Install Docker Support

if [ ! -f /usr/bin/docker ]; then
    echo "Docker Not Found - Installing Docker Support"
    yum install -y yum-utils \
      device-mapper-persistent-data \
      lvm2
    yum-config-manager \
        --add-repo \
        https://download.docker.com/linux/centos/docker-ce.repo
    yum -y install docker-ce-18.06.2.ce-3.el7 containerd.io
    systemctl start docker
    systemctl enable docker
else
    printf "Docker installation check \t[ OK ]\n"
fi

# Install IBM-ICP CE

echo "Installing IBM ICP-CE Docker Package"

docker pull ibmcom/icp-inception:3.1.1

# Stop firewall:
systemctl stop firewalld
systemctl disable firewalld

[ -d /opt/ibm-cloud-private-ce-3.1.1/ ] || mkdir -p /opt/ibm-cloud-private-ce-3.1.1/
cd /opt/ibm-cloud-private-ce-3.1.1/
docker run -e LICENSE=accept -v "$(pwd)":/data ibmcom/icp-inception:3.1.1 cp -r cluster /data


# Configure SSH Keys

if [ ! -f /root/.ssh/id_rsa ]; then
    echo "Generating SSH KEYS"
    mkdir -p /root/.ssh
    chmod 700 /root/.ssh
    cd /root/.ssh/ || exit 1
    cat /dev/zero | /usr/bin/ssh-keygen -q -N "" || exit 1
    cat id_rsa.pub >> authorized_keys || exit 1
    cp id_rsa /opt/ibm-cloud-private-ce-3.1.1/cluster/ssh_key || exit 1
else
    cp /root/.ssh/id_rsa /opt/ibm-cloud-private-ce-3.1.1/cluster/ssh_key || exit 1
    printf "SSH Key configuration \t[ OK ]\n"
fi


# Configure /etc/hosts

IP_ADDR_LISTED=$(grep -c $IP_ADDR /etc/hosts)

if [ $IP_ADDR_LISTED == 0 ]; then
    echo "Configuring /etc/hosts"
    # remove the entry for 127.0.1.1 as it breaks the docker installer
    perl -pi -e 's|^127.0.1.1.*$||' /etc/hosts
    printf "$IP_ADDR\t$HOSTNAME\n\n" >> /etc/hosts
else
    printf "/etc/hosts configuration \t[ OK ]\n"
fi

# Configure Ansible hosts file

cat << EOF > /opt/ibm-cloud-private-ce-3.1.1/cluster/hosts
[master]
$IP_ADDR
[worker]
$IP_ADDR
[proxy]
$IP_ADDR
#[management]
# 127.0.0.1
#[va]
#5.5.5.5
EOF

# Disabling unneeded services
cp /opt/ibm-cloud-private-ce-3.1.1/cluster/config.yaml /opt/ibm-cloud-private-ce-3.1.1/cluster/config.yaml.orig
sed -i.bak '/^management_services:/a \
' /opt/ibm-cloud-private-ce-3.1.1/cluster/config.yaml
# Fix loopback DNS:
echo "loopback_dns: true" >> /opt/ibm-cloud-private-ce-3.1.1/cluster/config.yaml


# Run Kubernetes Installer
printf "Running Kubernetes Installer...\n\n"
cd /opt/ibm-cloud-private-ce-3.1.1/cluster/ || exit 1
docker run -e LICENSE=accept --net=host -t -v "$(pwd)":/installer/cluster ibmcom/icp-inception:3.1.1 install

# Copy docker cert and make it available for later jenkins installation which will need it in order to authenticate to the ICP docker registry:
cp /etc/docker/certs.d/mycluster.icp:8500/ca.crt /tmp
chmod 777 /tmp/ca.crt
adduser tempuser
mkdir /home/tempuser/.ssh
chown tempuser:tempuser /home/tempuser/.ssh
chmod 700 /home/tempuser/.ssh
cat <<EOF > /home/tempuser/.ssh/authorized_keys
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDXSau1s+6rh2xWEoAg8AJ8jlR2vXWzL+QDKYbFmMyLvoMmkGD6AjlwsE2zOZ3ewMpR1zsyC9iDLCBCz1M3pg+9+mNxszyPKyRRxOoITwgn+wHspm6TlnwTjh9wxy95/ciUO/onvxhL2+A7LUWyVQjxUX12g/dZ0SDVSnpmMeqqSys41lWNwHtW0WJNvyjsICeoRs2SYvhZHJ0K51Ig9HUhQqy1d6a6AWJlpLDIzdl1H4jskZGIZGad+GyYoxhqEk/KHiiGKIzc+g5roUSWvKuDxjAb37p7mUblrkoOBXMM5n64IzWAObQN4McEZT4wrDAQ7gIxTC3vyGJDgykM1u4/ rickhenriksen@Ricks-MacBook-Pro.local
EOF
chmod 600 /home/tempuser/.ssh/authorized_keys
chown tempuser:tempuser /home/tempuser/.ssh/authorized_keys