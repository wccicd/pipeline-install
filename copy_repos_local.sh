#
#

echo "Mirroring WC CI/CD repositories..."
source_git=https://gitlab.com/wccicd/
for i in wcbuild crs search-app ts-web ts-utils xc-app ts-app helmcharts; do
   git clone $source_git/$i.git
done
