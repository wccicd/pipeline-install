echo "Mirroring WC CI/CD repositories..."
source_git=https://gitlab.com/wccicd/
host=`hostname`
target_git=ssh://git@$host:30022/wccicd
for i in wcbuild crs search-app ts-web ts-utils xc-app ts-app helmcharts; do
   git clone --bare $source_git/$i.git
   cd $i.git
   git push --mirror $target_git/$i.git
   cd ..
done