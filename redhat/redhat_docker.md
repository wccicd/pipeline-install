# Install Docker in AWS RedHat 7
1. login as root

1. `yum repolist disabled`

1. `yum-config-manager --enable "Red Hat Enterprise Linux 7 Server - Extras from RHUI (RPMs)"`

1. `yum search docker`

   Ensure that you see the main package:

   `docker-ce.x86_64`

1. `yum -y install docker-ce`

1. `systemctl enable docker`

1. `systemctl start docker`

1. `systemctl status docker`

1. To enable other users like **ec2-user** to use the docker commands:

   1. `groupadd docker`
   1. `usermod -aG docker ec2-user`
   
