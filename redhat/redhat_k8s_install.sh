#!/bin/bash
#
# Author: Angel Vera - angel.vera@hcl.com 
# Last updated: 02/12/2019
#
# Kudos for helping: Jason Hall@HCL
#
# This is install script inspired by: https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/#before-you-begin
# It was developed for RED HAT 7.
 

TARGET_OS='Red Hat Enterprise Linux Server'
HOSTNAME=$(/bin/hostname)
IP_ADDR=$(ip route get 1 | head -1 | awk '{print $7}')
DOCKER_USER=admin
DOCKER_PW=admin

PROGNAME=$(basename $0)

APT="/bin/yum"

function error_exit
{
    echo "Error: ${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

echo "************************************************"
echo "         Starting kubeadm Install          "
printf "************************************************\n\n"


# Check for OS Compatability
OS_CHECK=$(grep -c "$TARGET_OS" /etc/redhat-release)

if [ $OS_CHECK != 1 ]; then
    error_exit "$LINENO: Unsupported operating system"
else
    printf "OS Compatability check \t[ OK ]\n"
fi


# Update yum repo
echo "Updating yum repositories"

$APT -y update


########
# Update sysctl net.ipv4.ip_local_port_range
########
IPV4_PORT_RANGE=$(grep -c "^net.ipv4.ip_local_port_range" /etc/sysctl.conf)

if [ $IPV4_PORT_RANGE == 0 ]; then
    echo "setting sysctl net.ipv4.ip_local_port_range -> 10240  60999"
    echo 'net.ipv4.ip_local_port_range="10240 60999"' | tee -a /etc/sysctl.conf
    /sbin/sysctl -w net.ipv4.ip_local_port_range="10240  60999"
else
    printf "IPV4 Port Range \t[ OK ]\n"
fi


########
# Install Docker
########
if [ ! -f /usr/bin/docker ]; then
    echo "Docker Not Found - Installing Docker Support"
    yum install -y yum-utils \
      device-mapper-persistent-data \
      lvm2
    yum-config-manager \
        --enable  \
        "Red Hat Enterprise Linux 7 Server - Extras from RHUI (RPMs)"
    yum -y install docker-ce containerd.io
    systemctl start docker
    systemctl enable docker
else
    printf "Docker installation check \t[ OK ]\n"
fi


############
# Install kubeadm kubelet and kubectl
############
cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF

# Set SELinux in permissive mode (effectively disabling it)
setenforce 0
sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config

yum install -y kubelet kubeadm kubectl --disableexcludes=kubernetes

systemctl enable --now kubelet


#############
# Set sysctl for the iptables
#############
cat <<EOF >  /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
sysctl --system



#############
# Ensure br_netfilter is loaded
#############

IS_BR_NETFILTER_ON=$(lsmod |grep ^br_netfilter)
if [ $IS_BR_NETFILTER_ON == 0 ]; then
    error_on_exit "$LINENO: br_netfilter module not found. This module is required, install the module and retry, refer to: https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/#verify-the-mac-address-and-product-uuid-are-unique-for-every-node"
fi



###########
# Restarting daemons
###########
echo "$PROGNAME : Restaring daemons"
systemctl daemon-reload
systemctl restart kubelet



###########
# END
###########
echo "$PROGNAME : From here on refer to: https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/create-cluster-kubeadm/"
echo " "
echo " It is time to start your k8s cluster: "
echo " [to start up the cluster]"
echo "     kubeadm init"
echo " [to shutdown the cluster]"
echo "     kubectl drain <node name> --delete-local-data --force --ignore-daemonsets" 
echo "     kubectl delete node <node name>"
echo "     kubeadm reset"
echo "     iptables -F && iptables -t nat -F && iptables -t mangle -F && iptables -X"

