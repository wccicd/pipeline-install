# WCS Install Install FAQS

The information is here is complementary to, the formal trouble shooting instructions are provided here:
[https://help.hcltechsw.com/commerce/9.0.0/install/tasks/tig_deploykubern.html#helm_charts__context_qpm_vtj_jhb](https://help.hcltechsw.com/commerce/9.0.0/install/tasks/tig_deploykubern.html#helm_charts__context_qpm_vtj_jhb)


## Errors during Helm install
Use `helm install ecomdevauth ./ --debug` to understand what is happening and debug into the error.

## Using an unsecure remote docker registry
When using an unsecure remote docker registry you might see errors during helm install. Use `kubectl describe pod <NAME_OF_PREINSTALL_POD>` to look at the error.

You might see something like:
```
 Warning  Failed     16s (x2 over 33s)  kubelet, ip-10-190-176-228.ec2.internal  Error: ErrImagePull
  Normal   BackOff    5s (x3 over 32s)   kubelet, ip-10-190-176-228.ec2.internal  Back-off pulling image "10.190.176.228:5000/commerce/supportcontainer:1.9.6"
  Warning  Failed     5s (x3 over 32s)   kubelet, ip-10-190-176-228.ec2.internal  Error: ImagePullBackOff
```

1. In the docker client and all the nodes of k8s,
1. edit `/etc/docker/daemon.json` and add an entry like:
```
{
   "insecure-registries": ["10.190.176.228:5000"]
}
```
1. restart the docker client service
1. Repeat the helm install


## Handy deletion process of a helm failed installed
1. `helm delete ecom-dev-auth -n commerce`
1. `kubectl delete jobs --all -n commerce`
1. `kubectl delete events --all -n commerce`

## Is there a place where you document all the Vault values that are required?
Yes, [https://help.hcltechsw.com/commerce/9.0.0/install/refs/rigvaultmetadata.html?hl=vault%2Cenvironment](https://help.hcltechsw.com/commerce/9.0.0/install/refs/rigvaultmetadata.html?hl=vault%2Cenvironment)

## All the pods in the cluster keep getting restarted
:information: Notice that this FAQ only applies when all pods are affected, including those that form part of the namespace 'kube-system', which are services required for the pods to work correctly inside of the k8s cluster.

If you noticed that all the pods (including the `-n kube-system`) are getting constantly restarted this may point out to a inestability in your cluster. You can reach to for question: https://app.slack.com/client/T09NY5SBT/C09NXKJKA

One thing that can help is to reset the cluster by doing:
:information: this will not erase the container in `docker image ls` but it will delete all your pods.

```
kubeadm --reset
kubeadm init --pod-network-cidr=192.168.0.0/16
```
And then setup:
https://kubernetes.io/docs/setup/production-environment/container-runtimes/

And apply the calico plugin, if you are using another plugin you will need to adjust the kubeadm parameters.
```
kubectl apply -f https://docs.projectcalico.org/v3.11/manifests/calico.yaml
```



** Work in progress... **
