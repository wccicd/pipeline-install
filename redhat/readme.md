# How to install Commerce in Generic k8s

## Install Kubernetes with Calico
Following the instructions at:
[https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/create-cluster-kubeadm/#instructions](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/create-cluster-kubeadm/#instructions).

We created a scripts that takes you to the point where you will need to issue `kubeadm init`, see below:

1. Download [redhat_k8s_install.sh](redhat_k8s_install.sh)

1. `./redhat_k8s_install.sh`

1. `kubeadm init --pod-network-cidr=192.168.0.0/16`

1. Configure the root user to connect to the cluster
   ```
   mkdir -p $HOME/.kube
   sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
   sudo chown $(id -u):$(id -g) $HOME/.kube/config
   ```

1. `kubectl apply -f https://docs.projectcalico.org/v3.8/manifests/calico.yaml`

1. To ensure things are going well, run: `kubectl get pods --all-namespaces` and fix any problems that you might see.

## Prepare the cluster for Commerce
### Setup the roles
1. Download [clusterrole.yaml](clusterrole.yaml)

1. `kubectl create -f clusterrole.yaml`

### Download **Helm 3**
We recommend to run with helm 3. It is a straight forward download and copy the binary to the `/usr/local/bin/`

* To find out the version of helm installed `helm version`
* To find out the directory where helm is install `which helm`
* Find the binary and install instructions at: [https://helm.sh/docs/intro/install/](https://helm.sh/docs/intro/install/)


### Install and Configure nginx ingress
#### Install
1. `helm repo add stable https://kubernetes-charts.storage.googleapis.com/`

1. `helm install cluster stable/nginx-ingress --set rbac.create=true --set controller.extraArgs.enable-ssl-passthrough=""`

Use the following command with the `-w` to watch the status of the deployment. To exit break the process using CRTL+C:

```
kubectl --namespace default get services -o wide -w cluster-nginx-ingress-controller
```

1. record the IP of the master node with `ifconfig |grep -i inet`

1. Add the external IP in the ingress config using: `kubectl edit svc cluster-nginx-ingress-controller`

1. Look for the section: `clusterIP:` and following that section add:
  ```
  externalIPs:
  - 192.168.0.40
  ````

  The end result should be something like:
  ```
  spec:
  clusterIP: 10.104.227.17
  externalIPs:
  - 192.168.0.40
  ```
#### Taint ingress / Single node cluster

In the case that you are setting up a single cluster Kubernetes you may notice that the ingress pod remains in *pending* status.
The reason for the *pending* status is because by default kubernetes will not schedule pods on the master node for security reasons.

You must modify the configuration of the node to allow scheduling of pods in the master node.

1. `kubectl get nodes`
1. `kubect describe nodes <your_node_name> | grep -i taint`
- notice the property:
   ```
   Taints:             node-role.kubernetes.io/master:NoSchedule
   ```
- We need to remove this property to allow scheduling of pods in the master node., run step 3 for that action.
3. `kubectl taint nodes --all node-role.kubernetes.io/master-`


#### Configure
Add some specific for Commerce deployments related to SSL  

1. `kubectl edit ingress -n commerce`

1.  Replace or add the following annotations:

   ```
   annotations:
    ingress.kubernetes.io/secure-backends: "true"
    nginx.ingress.kubernetes.io/secure-backends: "true"
   ```

   With these annotations:
   ```
   annotations:
    kubernetes.io/ingress.class: nginx
    nginx.ingress.kubernetes.io/ssl-passthrough: "true"
   ```


### Create and Setup the ***commerce*** namespaces
1. `kubectl create namespace commerce`

1. Download [rbac.yaml](../rbac.yaml)

1. `kubectl create -f rbac.yaml -n commerce`

1. Download one of the following:

   |   k8s version   |  file   |
   | --- | --- |
   | pre 1.16 | [policy.yaml](https://gitlab.com/wccicd/pipeline-install/blob/6a5e7be7e3f86188062a8ca7a8e96bc1d7ce2f26/security.yaml) |
   | +1.16 | [policy.yaml](../policy.yaml) |

1. `kubectl create -f policy.yaml -n commerce`

### Start up the  Commerce Support container
> Adapted from ***Before you begin***, step 5 at: [https://help.hcltechsw.com/commerce/9.0.0/install/tasks/tig_deploykubern.html](https://help.hcltechsw.com/commerce/9.0.0/install/tasks/tig_deploykubern.html))

1. `curl https://help.hcltechsw.com/commerce/9.0.0/install/code/kubernetes/SupportContainer_1.0.0.tar.gz -o SupportContainer_1.0.0.tar.gz`
1. `docker load –i supportcontainer-1.0.0.tar`
1. **[optional]** at this point you tag and push the image  to a  private repository
   1. `docker tag supportcontainer:1.0.0 localhost:5000/commerce/supportcontainer`
   1. `docker push localhost:5000/commerce/supportcontainer`

### Start up the DB2 container (only for DEV purposes)
> Adapted from: [https://help.hcltechsw.com/commerce/9.0.0/install/tasks/tigusedb2docker_run.html](https://help.hcltechsw.com/commerce/9.0.0/install/tasks/tigusedb2docker_run.html)

1. Download the preconfigured (only for development/testing) db2 docker image that comes with the product
1. `docker load -i HCL_COMMERCE_9018_DB2_ENT_PRO.tgz`
1. `docker run -d --name ts-db -e LICENSE=accept --privileged -p 50000:50000 commerce/ts-db:9.0.1.8`


`NOTE TO MYSELF`
I think we can mount the /home/db2inst1 to a volumn and always persist changes

## Modify the helm charts for Consul/Vault
> Adapted from ***Procedure***, step 1 at : [https://help.hcltechsw.com/commerce/9.0.0/install/tasks/tig_deploykubern.html#helm_charts__context_qpm_vtj_jhb](https://help.hcltechsw.com/commerce/9.0.0/install/tasks/tig_deploykubern.html#helm_charts__context_qpm_vtj_jhb)

1. download the corresponding Helm chart if you haven't
1. unzip the file
1. `vi HelmCharts/Vault/Vault_Consul/env.profile`
1. :exclamation:In this file you setup the initial values of the DB you are connecting to. Edit the variables according to [https://help.hcltechsw.com/commerce/9.0.0/install/tasks/tig_deploykubern.html#helm_charts__context_qpm_vtj_jhb](https://help.hcltechsw.com/commerce/9.0.0/install/tasks/tig_deploykubern.html#helm_charts__context_qpm_vtj_jhb)
1. `cd HelmCharts/Vault/`
1. Depending on the k8s version you might need to  modify the `deployment.yaml`

   |   k8s version   |  update this | with that |
   | --- | --- | --- |
   | pre 1.16 | no changes |  |
   | +1.16 | `apiVersion: extensions/v1beta1` | `apps/v1`

1. `./deploy_vault.sh`
   > **NOTE**: If you run into problems you can always delete the created resources one at a time

   > **NOTE2**: If you are deploying to a specific namespace other than the default, make sure to run `kubectl config set-context --current --namespace <NAMESPACE>``

   > tips: `kubectl delete service vault-consul`

   > tips: `kubectl delete deployment vault.consul`


1. `IMPORTANT:` Follow the steps in the helpcenter ([https://help.hcltechsw.com/commerce/9.0.0/install/tasks/tig_deploykubern.html#helm_charts__context_qpm_vtj_jhb](https://help.hcltechsw.com/commerce/9.0.0/install/tasks/tig_deploykubern.html#helm_charts__context_qpm_vtj_jhb))to validate that vault installed correctly and record the information for the key and token.


### Prepare encrypted passwords
1. `docker load -i HCL_COMMERCE_9018_UTILITY_ENT.tgz`
1. `docker run -d --name ts-utils -e LICENSE=accept commerce/ts-utils:9.0.1.8`
1. `docker start ts-utils`
1. `docker exec -it ts-utils bash`
1. `cd /opt/WebSphere/CommerceServer90/bin/`
1. `./wcs_encrypt passw0rd` Make sure you write down the value
1. `./wcs_encrypt key_encryption merchant_key` Make sure you write down the value


## Deploy Commerce
1. Download all the other 5 images of commerce.

   ```
   HCL_COMMERCE_9018_IHS_ENT_PRO.tgz
   HCL_COMMERCE_9018_SEARCH_ENT_PRO.tgz
   HCL_COMMERCE_9018_STORE_ENT_PRO.tgz
   HCL_COMMERCE_9018_TS_ENT_PRO.tgz
   HCL_COMMERCE_9018_XC_ENT_PRO.tgz
   ```

1. Load the images:

   ```
   docker load -i HCL_COMMERCE_9018_IHS_ENT_PRO.tgz
   docker load -i HCL_COMMERCE_9018_SEARCH_ENT_PRO.tgz
   docker load -i HCL_COMMERCE_9018_STORE_ENT_PRO.tgz
   docker load -i HCL_COMMERCE_9018_TS_ENT_PRO.tgz
   docker load -i HCL_COMMERCE_9018_XC_ENT_PRO.tgz
   ```

1. Using the Helm charts that come with the product or the helm charts available here: https://gitlab.com/wccicd/helmcharts
1. copy the charts locally to `/root/gitrepo/helmcharts`
1. Modify the helm charts as needed for vault token, imageRepo, and other values, inspect them carefuly.
   1. ***+9.0.1.8*** the charts require of the vaultToken stored in a K8S Secrets inside of the values.yaml as vaultTokenSecret ([How to create secrets](https://kubernetes.io/docs/concepts/configuration/secret/))
   1. `echo -n "_VAULT_TOKEN_"|base64`
   :exclamation: note the -n in the echo to remove all new lines characters
   1. modify the [secret_vault_token.yaml](secret_vault_token.yaml)
   1. `kubectl apply -f ./secret_vault_token.yaml -n commerce`
   1. modify the local helm charts for auth/live to have `vaultTokenSecret: vaultsecret`

   :exclamation: if using a unsecure registry which is not recommended see [Install_FAQ.md](install_FAQ.md)
1. `cd /root/gitrepo/helmcharts/auth`
1. `helm install ecom-dev-auth ./ -n commerce`

## Installation is now complete
Your auth environments should now be up and running, if you run into problems. Read the FAQ.

## Links

# FAQ / Debug
[Install_FAQ.md](install_FAQ.md)
